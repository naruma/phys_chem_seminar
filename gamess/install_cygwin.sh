#!/bin/bash

    mkdir $HOME/scr
    mv runall runall.junk
    sed -e "s/\&  exam/\&  logs\/exam/" runall.junk > runall
    rm -f runall.junk

    cd ddi
    cp compddi compddi.junk
    sed -e "s/TARGET = ibm64/TARGET=linux64/"  \
	        -e "s/set SYSV = true/set SYSV = false/" compddi.junk > compddi
#    rm -f compddi.junk
    chmod a+x compddi
    ./compddi >& compddi.log
    cp -p ddikick.x ../
    cd ..
    ./compall
    ./lked gamess "current" >& lked.log
    tail -n 20 ddi/compddi.log
    more lked.log
