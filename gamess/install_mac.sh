#!/bin/bash

    mkdir $HOME/scr
    cd ddi
    ./compddi >& compddi.log
    cp -p ddikick.x ../
    cd ..
    ./compall
    ./lked gamess "current" >& lked.log
    tail -n 20 ddi/compddi.log
    more lked.log
