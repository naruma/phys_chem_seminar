#!/bin/sh
export GMSPATH=`pwd`/../../../gamess
if [ -f $HOME/scr/CO_RHF.* ] ; then
    rm $HOME/scr/CO_RHF.*
fi
${GMSPATH}/rungms CO_RHF.inp current >& CO_RHF.out
