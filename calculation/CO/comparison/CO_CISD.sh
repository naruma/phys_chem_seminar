#!/bin/sh
export GMSPATH=`pwd`/../../../gamess
if [ -f $HOME/scr/CO_CISD.* ] ; then
   rm $HOME/scr/CO_CISD.*
fi
${GMSPATH}/rungms CO_CISD.inp current >& CO_CISD.out
