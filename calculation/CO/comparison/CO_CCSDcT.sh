#!/bin/sh
export GMSPATH=`pwd`/../../../gamess
if [ -f $HOME/scr/CO_CCSDcT.* ] ; then
   rm $HOME/scr/CO_CCSDcT.*
fi
${GMSPATH}/rungms CO_CCSDcT.inp current >& CO_CCSDcT.out
